/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordinario;

/**
 *
 * @author fresh-fresh
 */
        public class Recibo {
                private int numRecibo;
                private String nombre;
                private int puesto;
                private int nivel;
                private int dTrabajados;
            
        public Recibo (){
                this.numRecibo = 0;
                this.nombre = "";
                this.puesto = 0;
                this.nivel = 0;
                this.dTrabajados = 0;
      
}
        public Recibo(int numRecibo, String nombre, int puesto, int nivel, int dTrabajados){
            this.numRecibo = numRecibo;
            this.nombre = nombre;
            this.puesto = puesto;
            this.nivel = nivel;
            this.dTrabajados = dTrabajados;
            
        }
        
        public Recibo (Recibo otro){
                this.numRecibo = otro.numRecibo;
                this.nombre = otro.nombre;
                this.puesto = otro.puesto;
                this.nivel = otro.nivel;
                this.dTrabajados = otro.dTrabajados;
        }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getdTrabajados() {
        return dTrabajados;
    }

    public void setdTrabajados(int dTrabajados) {
        this.dTrabajados = dTrabajados;
    }
        
        
    
    //Metodos de comportamiento
    
    public float calcularPago (){
        float calPago = 0.0f;
        
        if(puesto == 1){
         calPago = this.dTrabajados * 100.00f;
     }
     
     if(puesto == 2){
         calPago = this.dTrabajados * 200.00f;
     }
     
     if(puesto == 3){
         calPago = this.dTrabajados * 300.00f;
     }
        
    return calPago;
    }
      
    public float calcularImpuesto (){
        float calImpuesto = 0.0f;
        float impuesto = 0.0f;
        
        if(nivel == 1){
        impuesto = 0.03f;
            calImpuesto = this.calcularPago() * impuesto;
     }
     
     if(nivel == 2){
         impuesto = 0.05f;
            calImpuesto = this.calcularPago() * impuesto;
     }
        
      /*  float impuesto = 0.16f;
      calImpuesto = this.calcularSubtotal() * impuesto;
      */
      
      
      return calImpuesto;
    }
    
    public float TotalaPagar (){
        float calTotal = 0.0f;
        
        calTotal = this.calcularPago() - this.calcularImpuesto();
        
       return calTotal; 
    }
}
